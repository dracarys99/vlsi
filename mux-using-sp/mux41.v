`include "mux21.v"
module mux41(input a,input b,input c,input d,input[1:0] s,output out);
    wire a,b,c,d,out;
    wire[1:0] s;
    wire o1,o2;
    mux21_sp m1(.a(a),.b(b),.s(s[0]),.out(o1));
    mux21_sp m2(.a(c),.b(d),.s(s[0]),.out(o2));
    mux21_sp m3(.a(o1),.b(o2),.s(s[1]),.out(out)); 
endmodule
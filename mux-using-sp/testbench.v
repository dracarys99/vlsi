`include "mux41.v"
module testbench();
reg a,b,c,d;
wire to,m2,m4;
reg[1:0] s;
trans_gate t1(a,s[0],to);
mux21_sp m1(a,b,s[0],m2);
mux41 m9(a,b,c,d,s,m4);
initial begin
  a=0;
  b=0;
  c=0;
  d=0;
  s=00;
  #35 $finish;
end
always #1 a=~a;
always #2 b=~b;
always #3 c=~c;
always #4 d=~d;
always #5 s[0]=~s[0];
always #7 s[1]=~s[1];
always @(a)
    $display("%b %b %b %b %b t0:%b m2:%b m4:%b",a,b,c,d,s,to,m2,m4);
endmodule
module bitadd_32(input wire[31:0] a,input wire[31:0] b,output wire[31:0] sum,output wire cout);
    wire[32:0] res;
    assign res = {1'b0,a} + {1'b0,b};
    assign cout = res[32];
    assign sum = res[31:0];
endmodule

module bitsub_32(input wire[31:0] a,input wire[31:0] b,output wire[31:0] sum,output wire cout);
    wire[32:0] res;
    assign res = {1'b0,a} - {1'b0,b};
    assign cout = res[32];
    assign sum = res[31:0];
endmodule

module bitmul_16(input wire[15:0] a,input wire[15:0] b,output wire[31:0] sum);
    assign sum = a * b;
endmodule

module mux2x1(input wire a,input wire b,input wire s,output wire out);
    assign out = s ? b : a;
endmodule

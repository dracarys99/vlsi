module mux8x1(input wire en,input wire[7:0] in,input wire[2:0] s,output reg out);
always @(*)
    if(en)
        case(s)
            3'b000:out=in[0];
            3'b001:out=in[1];
            3'b010:out=in[2];
            3'b011:out=in[3];
            3'b100:out=in[4];
            3'b101:out=in[5];
            3'b110:out=in[6];
            3'b111:out=in[7];
        endcase
endmodule

module demux8x1(input wire en,input wire in,input wire[2:0] s,output reg[7:0] out);
always@(*)
    begin
    out=8'bxxxxxxxx;
    if(en)
        case(s)
            3'b000:out[0]=in;
            3'b001:out[1]=in;
            3'b010:out[2]=in;
            3'b011:out[3]=in;
            3'b100:out[4]=in;
            3'b101:out[5]=in;
            3'b110:out[6]=in;
            3'b111:out[7]=in;
        endcase
    end
endmodule
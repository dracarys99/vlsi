`ifndef CLOCK
`define CLOCK
module timer(output clk);
reg clk;
initial clk=0;
always #1
  clk=~clk;
endmodule
`endif

`ifndef xor_switch_cmos
`define xor_switch_cmos
//`include"switch_not.v"
module xor_switch(input a,input b,output y);
    wire w1,w2,w3,na,nb;
    supply0 gnd;
    supply1 vdd;
    not_switch n1(.y(na),.a(a));
    not_switch n2(.y(nb),.a(b));
    nmos(w3,gnd,na);
    nmos(w3,gnd,b);
    nmos(y,w3,nb);
    nmos(y,w3,a);
    pmos(w2,vdd,na);
    pmos(w1,vdd,nb);
    pmos(y,w1,a);
    pmos(y,w2,b);
endmodule
`endif
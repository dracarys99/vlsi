`include "switchp_nand.v"
`include "switchp_nor.v"
`include "switchp_xor.v"
`include "switchp_xnor.v"
`include "switchp_not.v"
//`include "clock.v"
module testbench();
    wire na,no,xo,xno,an,o;
    reg a=1,b=1,c=0;
    //timer t1(.clk(clk));
    xor_switch  n1(.a(a),.b(b),.y(xo));
    xnor_switch n2(.a(a),.b(b),.y(xno));
    nand_switch n3(.a(a),.b(b),.y(na));
    nor_switch  n4(.a(a),.b(b),.y(no));
    not_switch  n5(.a(na),.y(an));
    not_switch  n6(.a(no),.y(o));
    always @(a or ~a)begin
        $display("%0t %b %b nand:%b nor:%b xor:%b xnor:%b and:%b or:%b",$time,a,b,na,no,xo,xno,an,o);
        if(b==1'b1)begin
            if(a==1'b1)
                $finish;
            else
                a=1'b1;
            b=1'b0;
        end
        else
            b=1'b1;
    end
    //initial begin
    //$monitor("%0t %b %b nand:%b nor:%b xor:%b xnor:%b and:%b or:%b",$time,a,b,na,no,xo,xno,an,o);
    //end
endmodule




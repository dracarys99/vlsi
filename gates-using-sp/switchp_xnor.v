`ifndef xnor_switch_cmos
`define xnor_switch_cmos
//`include"switch_not.v"
module xnor_switch(input a,input b,output y);
    wire w1,w2,w3,na,nb;
    supply0 gnd;
    supply1 vdd;
    not_switch n1(.y(na),.a(a));
    not_switch n2(.y(nb),.a(b));
    nmos(w2,gnd,na);
    nmos(w1,gnd,nb);
    nmos(y,w1,a);
    nmos(y,w2,b);
    pmos(w3,vdd,na);
    pmos(w3,vdd,b);
    pmos(y,w3,nb);
    pmos(y,w3,a);
endmodule
`endif
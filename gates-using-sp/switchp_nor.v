`ifndef nor_switch_cmos
`define nor_switch_cmos
module nor_switch(input a,input b,output y);
wire w;
supply0 gnd;
supply1 vdd;
nmos(y,gnd,b);
nmos(y,gnd,a);
pmos(w,vdd,b);
pmos(y,w,a);
endmodule
`endif
`include "../clock.v"
`include "d2c.v"
`include "counter.v"
`include "flips.v"

module testbench();
    wire clk;
    wire[7:0] c1;
    wire[3:0] c2;
    reg enable,sel,reset;
    timer clock(clk);
    updcounter co1(clk,enable,sel,reset,c1);
    mod2counter co2(clk,enable,reset,c2);
    initial begin
        $display("  : e r s c1\tc2");
        enable=1'b0;
        sel=1'b0;
        reset=1'b0;
        #2;
        enable=1'b1;
        #16;
        sel=1'b1;
        #16
        reset=1'b1;
        #1;
        $finish;
    end
    always #2
        $display("%0t: %b %b %b %b\t%b",$time,enable,reset,sel,c1,c2);
endmodule
/*
module testbench_jk();
    reg j,k,reset;
    wire clk;
    timer clock(clk);
    wire q,q1;
    dflipflop ff(j,clk,q,q1);
    initial begin
        reset=1'b1;
        #2
        reset=1'b0;
        j=1'b0;
        k=1'b0;
        #2
        k=1'b1;
        #2
        j=1'b1;
        k=1'b0;
        #2
        k=1'b1;
        #2
        $finish;
    end
    always #2
        $display("%0t: %b %b %b",$time,j,q,q1);
endmodule
*/
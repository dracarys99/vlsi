`ifndef COUNTER
`define COUNTER
module updcounter(input wire clk,input wire enable, input wire sel,input wire reset,output reg[7:0] val);
    initial
        val<=8'b00000000;
    always@(posedge clk) begin
        if(reset)
                val<=8'b00000000;
        else if(enable)
                if(sel)
                    if(val==8'b00000000)
                        val<=8'b11111111;
                    else
                        val<=val-1;
                else
                    if(val==8'b11111111)
                        val<=8'b00000000;
                    else
                        val<=val+1;
    end
endmodule
`endif

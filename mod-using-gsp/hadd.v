`ifndef hadd_switch_cmos
`define hadd_switch_cmos
`include "../gates-using-sp/switchp_nand.v"
`include "../gates-using-sp/switchp_nor.v"
`include "../gates-using-sp/switchp_xor.v"
`include "../gates-using-sp/switchp_xnor.v"
`include "../gates-using-sp/switchp_not.v"
module hadd(input a,input b,output sum,output cout);
    wire a,b,sum,cout,na;
    xor_switch  n1(.a(a),.b(b),.y(sum));
    nand_switch n3(.a(a),.b(b),.y(na));
    not_switch  n5(.a(na),.y(cout));
endmodule
`endif
`include "fadd.v"
module tb();
reg a,b,c;
wire out,sum;
fadd f1(a,b,c,sum,out);
initial begin
  a=0;
  b=0;
  c=0;
  #40 $finish;
end
always #3 a=~a;
always #6 b=~b;
always #12 c=~c;
always@(a)
    $display("%b  %b  %b  %b  %b",a,b,c,out,sum);
endmodule

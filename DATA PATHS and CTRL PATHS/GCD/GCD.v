`include "Input.v"
`include "Operations.v"
`include "Combos.v"
`include "Utils.v"

module GCD_datapath ( lt , gt , eq , LdA , LdB , sel1 , sel2 , sel_in , data_in , clk);

    input LdA , LdB , sel1 , sel_in , sel2 , clk;
    input [15 : 0] data_in ;
    output lt , gt , eq ;
    wire [15 : 0] Bus, Aout , Bout , X , Y  , Subout ;

    PIPO1 A(Aout , Bus , LdA , clk);
    PIPO1 B(Bout , Bus , LdB , clk);
    // taken inputs

    MUX m1( X , Aout , Bout , sel1);
    MUX m2( Y , Aout , Bout , sel2);
    MUX main(Bus , Subout , data_in , sel_in);

    SUB sub(Subout , X , Y);

    COMPARE comp( lt , gt , eq , Aout , Bout);

endmodule


module GCD_ctrlpath ( lt , gt , eq , LdA , LdB , sel1 , sel2, sel_in , done, start , clk);

    input lt , gt , eq , start , clk ;
    output reg LdA , LdB , sel1 , sel2 , sel_in , done;

    reg [2 : 0] state ;

   parameter S0 = 3'b000 , S1 = 3'b001 ,S2 = 3'b010 , S3 = 3'b011 , S4 = 3'b100 , S5 = 3'b101;

   always @ ( posedge clk)
        begin
          case (state)
          S0: if (start)  state <= S1;
            S1: state <= S2;
            S2: #2 if (eq) state <= S5;
                    else if(lt) state <= S3;
                    else if(gt) state <= S4;
            
            S3: #2  if (eq) state <= S5;
                    else if (gt) state <= S4;
                
            S4: #2 if (eq) state <= S5;
                    else if (lt) state <= S3;

            S5: state <= S5;
            default : state <= S0;
          endcase
        end

    // this happens only if the state changes keep this in mind
    always @ (state)
        begin
            case (state)
                S0: begin #1 LdA = 1; LdB = 0 ; done =  0; end
                S1: begin #1 LdA = 0; LdB = 1 ; end
                
                S2: if ( eq ) done = 1;
                    else if( lt ) begin 
                                            sel1 = 1 ; sel2 = 0; sel_in = 0 ;
                                           #1 LdA = 0; LdB = 1 ;
                                  end
                     else if( gt ) begin 
                                            sel1 = 0 ; sel2 = 1; sel_in = 0 ;
                                           #1 LdA = 1; LdB = 0 ;
                                           end

                S3: if ( eq ) done = 1;
                    else if( lt ) begin 
                                            sel1 = 1 ; sel2 = 0; sel_in = 0 ;
                                           #1 LdA = 0; LdB = 1 ;
                                  end
                     else if( gt ) begin 
                                          sel1 = 0 ; sel2 = 1; sel_in = 0 ;
                                           #1 LdA = 1; LdB = 0 ;
                                           end

                S4:  if ( eq ) done = 1;
                    else if( lt ) begin 
                                            sel1 = 1 ; sel2 = 0; sel_in = 0 ;
                                           #1 LdA = 0; LdB = 1 ;
                                  end
                     else if( gt ) begin 
                                            sel1 = 0 ; sel2 = 1 ; sel_in = 0 ;
                                           #1 LdA = 1; LdB = 0 ;
                                           end
                     
                S5: begin #1 done = 1 ; LdB = 0 ;  sel1 = 0 ; sel2 = 0; end

                default: begin LdA = 0; LdB = 0 ;  end
            endcase
        end
endmodule


module GCD_tb();

    reg [15 : 0] data_in ;
    reg clk , start ;
    wire done ;

    GCD_datapath DP(lt , gt , eq , LdA , LdB , sel1 , sel2 , sel_in , data_in , clk);
    GCD_ctrlpath CON(lt , gt , eq , LdA , LdB , sel1 , sel2, sel_in , done, start , clk);

    initial 
        begin

        clk = 1'b0;
        #3 start = 1'b1;
        #500 $finish;

        end

    always #5 clk = ~clk ;

    initial 
        begin
             #17 data_in = 143;
             #10 data_in = 78;
             end
    
    initial 
        begin 
            $monitor ($time , " %d %b ",DP.Aout , done);
            $dumpfile ("gcd.vdc");
            $dumpvars ( 0 , GCD_tb);
        end
endmodule